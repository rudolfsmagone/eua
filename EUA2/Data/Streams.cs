﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace EUA2.Data
{
    public class Streams
    {
        public int Id { get; set; }
        [Required, StringLength(100)]
        public string StreamHeader { get; set; }
        public string StreamLink { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
