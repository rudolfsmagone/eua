﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace EUA2.Data
{
    public class Articles
    {
       
        public int Id { get; set; }
        [Required, StringLength(100)]
        public string ArticleName { get; set; }
        public string ArticleContext { get; set; }
        public string ArticleText { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool ImportantNews { get; set; }
    }
}
