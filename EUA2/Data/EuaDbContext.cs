﻿using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EUA2.Data
{
    public class EuaDbContext:DbContext
    {
    public EuaDbContext(DbContextOptions<EuaDbContext>options):base(options){}
    public DbSet<Articles> Articles { get; set; }
    public DbSet<Streams> Streams { get; set; }
    }
}
