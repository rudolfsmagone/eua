using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using EUA2.Data;
namespace EUA2.Pages
{
    public class ArticlesModel : PageModel
    {
        private readonly EuaDbContext _db;
        public ArticlesModel(EuaDbContext db)
        {
            _db = db;
        }
        public Articles Article { get; set; }
        public void OnGet(int id)
        {
            Article = _db.Articles.Single(art => art.Id == id);
            
        }
        
    }
}