﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.RazorPages;
using EUA2.Data;
namespace EUA2.Pages
{
    public class IndexModel : PageModel
    {
        private readonly EuaDbContext _db;
        public IndexModel(EuaDbContext db)
        {
            _db = db;
        }
        public IList<Articles> UnArticles { get; set; }
        public IList<Articles> ImArticles { get; set; }
        public void OnGet()
        {
            UnArticles = _db.Articles.OrderByDescending(o=>o.Id).Where(o => o.ImportantNews == false).ToList();
            ImArticles = _db.Articles.OrderByDescending(o => o.Id).Where(o => o.ImportantNews == true).ToList();

        }
    }
}
